if (typeof(AppExpSou) === 'undefined') {
    var AppExpSou = {};

    $(function () {


        var tag = document.createElement('script');
        tag.src = "//www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        var player;

        onYouTubeIframeAPIReady = function () {
            player = new YT.Player('player', {
                height: '246',
                width: '448',
                videoId: '2r9sfJMkkgg',  // youtube video id
                playerVars: {
                    'autoplay': 0,
                    'rel': 0,
                    'showinfo': 0
                },
                events: {
                    'onStateChange': onPlayerStateChange
                }
            });

            AppExpSou.YTplayer = player;


            $(function () {
                $('.video-item a').on('click', function (e) {
                    e.preventDefault();
                    //console.log('Changed video')
                    var videoId = $(this).attr('data-video-id');

                    $("#player").show();
                    $("#thumbnail_container").hide();
                    $('.start-hide').show()
                    $('.start-video').hide();

                    AppExpSou.YTplayer.loadVideoById(videoId);
                    AppExpSou.YTplayer.playVideo();

                    $("#thumbnail").attr('src', 'http://img.youtube.com/vi/' + videoId + '/mqdefault.jpg')
                })

            })


        }

        var p = document.getElementById("player");
        $(p).hide();

        var t = document.getElementById("thumbnail");
        t.src = "http://img.youtube.com/vi/2r9sfJMkkgg/mqdefault.jpg";

        onPlayerStateChange = function (event) {
            if (event.data == YT.PlayerState.ENDED) {
                $('.start-video').fadeIn('normal');
                $("#player").hide();
                $('#thumbnail_container').show()
                $('#thumbnail').show();
            }
        }

        $(document).on('click', '.start-video', function () {
            $(this).hide();
            $("#player").show();
            $("#thumbnail_container").hide();
            player.playVideo();
        });


        //
        //
        //$('.player, .player iframe, .video-item a').on('click', function(){
        //    console.log('Player clicked')
        //    $('.player-button').hide();
        //})
        //
        //$('.video-item a').on('click', function(e){
        //    e.preventDefault();
        //    console.log('Changed video')
        //    $('#video').attr('src',($(this).attr('data-video-url')));
        //})
        //-----------------------------------------------------------------------------------
        // Helpers
        //-----------------------------------------------------------------------------------
        AppExpSou.addLeftZeros = function (length, number) {
            var zeros = "";
            number = number.toString();
            for (var i = 0; i < length - number.length; i++) {
                zeros += "0";
            }
            ;
            return zeros + number;
        };

        AppExpSou.css3Prefix = (function (length, number) {

            var t = ["msTransform", "MozTransform", "WebkitTransform", "OTransform", "transform"],
                listValues = ["", "-ms-", "-moz-", "-webkit-", "-o-"],
                value = "";

            for (var test, i = 0; test = t[i++];) {
                if (typeof document.body.style[test] != "undefined") {
                    value = listValues[i];
                    break;
                }
            }
            ;

            return value;
        }());

        AppExpSou.resizeObserver = (function () {

            var list = [],
                list2 = [],
                height = false,
                display = $(window);

            display.resize(function () {
                height = display.height();
                $.each(list, function (i, callback) {
                    callback(height);
                });
                $.each(list2, function (i, callback) {
                    callback(height);
                });
            });

            return {
                addObserver: function (item) {
                    if (typeof(item) === 'function') {
                        list.push(item);
                    }
                },
                addObserverEnd: function (item) {
                    if (typeof(item) === 'function') {
                        list2.push(item);
                    }
                }
            };
        }());

        //-----------------------------------------------------------------------------------
        // Scroll
        //-----------------------------------------------------------------------------------

        /*
         * Custom Scroll
         *
         */
        AppExpSou.customScroll = (function () {

            var wrapper,
                fakeScroll = $('<div></div>'),
                to = 0,
                current = 0,
                windowHeight,
                top, movieRange;

            function init(pTop, pMovieRange) {

                top = pTop;
                movieRange = pMovieRange + 800;
                wrapper = $('#sou-general');

                // setup html
                setupScroll();

                // update position on user's scroll and resize
                $(window).scroll(function () {

                    var __positionScroll = $(window).scrollTop();
                    var __windowFullHeight = $('body div').first().height();

                    var percentage = parseInt(__positionScroll * 100 / __windowFullHeight);

                    switch (percentage) {
                        case 25:
                            ga('send', {
                                hitType: 'event',
                                eventCategory: 'sou',
                                eventAction: 'paralax',
                                eventLabel: '25%'
                            });
                            return;

                        case 50:
                            ga('send', {
                                hitType: 'event',
                                eventCategory: 'sou',
                                eventAction: 'paralax',
                                eventLabel: '50%'
                            });

                            return;

                        case 75:
                            ga('send', {
                                hitType: 'event',
                                eventCategory: 'sou',
                                eventAction: 'paralax',
                                eventLabel: '75%'
                            });
                            return;

                        case 100:
                            ga('send', {
                                hitType: 'event',
                                eventCategory: 'sou',
                                eventAction: 'paralax',
                                eventLabel: '100%'
                            });
                            return;

                    }


                    $('#bt-ver-produtos').on('click', function () {

                        ga('send', {
                            hitType: 'event',
                            eventCategory: 'sou',
                            eventAction: 'click',
                            eventLabel: 'ver-produtos',
                            transport: 'beacon'
                        });

                    });
                    to = $(this).scrollTop() >> 1;
                });

                // run interval
                setInterval(function () {
                    if (Math.round(current) !== to) {

                        current += (to - current) * 0.1;

                        // fix the content position
                        if (current > top && to < movieRange) {
                            wrapper.css('top', -top);
                        } else if (to > top + 500) {
                            wrapper.css('top', ((movieRange - to) << 1) - top);
                        } else {
                            wrapper.css('top', -current);
                        }

                        // notify observers
                        AppExpSou.scrollObserver.update(current);
                    }
                }, 12);
            };

            function setupScroll() {

                // set position to container
                wrapper.css({
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    position: 'fixed'
                });

                // add fake scroll
                $('body').prepend(fakeScroll);
            };

            function updateScroll() {

                // get window height
                windowHeight = $(window).height();
                fakeScroll.css('height', ((wrapper.height() - windowHeight + movieRange) << 1) + windowHeight);
            };

            /*
             * return
             */
            return {
                init: init,
                update: updateScroll,
                setScroll: function (top) {
                    $(window).scrollTop(top << 1);
                }
            };

        }());

        /*
         * Observer
         *
         */
        AppExpSou.scrollObserver = (function () {

            var list = [];

            return {
                addObserver: function (item) {
                    if (typeof(item) === 'function') {
                        list.push(item);
                    }
                },
                update: function (top) {
                    $.each(list, function (i, callback) {
                        callback(top);
                    });
                }
            };
        }());

        /*
         * Scroller
         *
         */
        AppExpSou.scroller = function (start, end, callback) {

            var dif = end - start,
                current = 0,
                currentTop = 0;

            // on Scroll
            AppExpSou.scrollObserver.addObserver(function (top) {
                currentTop = top;
                update(currentTop);
            });

            // on resize
            AppExpSou.resizeObserver.addObserverEnd(function (wHeight) {
                callback(current, true);
            });

            function update(top) {
                if (top < start) {
                    if (current !== 0) {
                        current = 0;
                        callback(current);
                    }
                    ;
                    return;
                }
                if (top < end) {
                    current = (100 / dif) * (top - start);
                    callback(current);
                    return;
                }
                if (current != 100) {
                    current = 100;
                    callback(current);
                    return;
                }
            }
        };

        //-----------------------------------------------------------------------------------
        // Movie Classes
        //-----------------------------------------------------------------------------------

        /*
         * Change Page
         *
         */
        AppExpSou.pageTransition = function (inner, windowHeight, transitionRange) {

            var _index = 0,
                _pages = [],
                _currentTop = 0,
                _percent,
                _listenerMove;

            // on resize
            AppExpSou.resizeObserver.addObserver(function (wHeight) {
                windowHeight = wHeight;
                updatePosition(_currentTop);
            });

            // on scroll 
            AppExpSou.scrollObserver.addObserver(function (top) {
                _currentTop = top;
                updatePosition(_currentTop);
            });

            function updatePosition(top) {

                for (var i = _pages.length - 1; i >= 0; i--) {
                    if (top > _pages[i].start && top < _pages[i].end) {
                        _percent = (top - _pages[i].start) / _pages[i].range;
                        inner.css('top', -(_pages[i].index * windowHeight) - (windowHeight * _percent));
                        if (_listenerMove) _listenerMove(_pages[i].index);
                        break;
                    }
                    if (i === _pages.length - 1 && top > _pages[i].end) {
                        inner.css('top', -((_pages[i].index + 1) * windowHeight));
                        if (_listenerMove) _listenerMove(_pages[i].index);
                        break;
                    }
                    if (top < _pages[i].start) {
                        inner.css('top', -(_pages[i].index * windowHeight));
                    }
                }
            };

            return {
                add: function (position, tRange, top) {

                    var i = _index,
                        range = tRange | transitionRange;

                    _pages.push({
                        index: _index,
                        start: position,
                        range: range,
                        end: position + range
                    });

                    _index++;
                    return range;
                },
                listenerPageMove: function (fn) {
                    _listenerMove = fn;
                }
            };
        };

        /*
         * Item Scroll
         *
         */
        AppExpSou.itemScroll = function (data) {

            var defaults = {
                dom: false,
                start: 0,
                end: 0,
                css: 'top',
                distance: false,
                percent: 100
            };

            mergeAndCalc(data);

            defaults.resize = function (newopts) {
                mergeAndCalc(newopts);
            }

            function mergeAndCalc(options) {
                $.extend(defaults, options);
                defaults.distance = defaults.end - defaults.start;
            }

            return defaults;
        };

        //-----------------------------------------------------------------------------------
        // Movie Pages
        //-----------------------------------------------------------------------------------

        /*
         * Movie Intro
         *
         */
        AppExpSou.Intro = function (position, transitionRange, btVerProdutos) {

            var _container = $('#expsou-intro'),
                _scroll = _container.find('.sou-scroll'),
                _banner = _scroll.find('.sou-banner'),
                _productsContainer = _scroll.find('.sou-products'),
                _defaultProdHeight = 438,
                _drop = _container.find('.sou-drop'),
                _dropText = _drop.find('span'),
                _pageDifference,
                _scrollRange,
                _perfectPosition;

            function resize(wHeight) {
                _pageDifference = fixProductsHeight(wHeight);
                _scrollRange = calcScrollRange(_pageDifference, wHeight);
                _perfectPosition = position + _scrollRange + (transitionRange * ((wHeight - _productsContainer.height()) / 2) / wHeight);
            };

            function calcScrollRange(distance, wHeight) {
                return (distance * transitionRange) / wHeight;
            };

            function fixProductsHeight(wHeight) {

                var availableHeight = wHeight - _banner.outerHeight(true),
                    pageDifference = 0;

                if (availableHeight > _defaultProdHeight) {
                    _productsContainer.css('height', 541);
                } else {
                    _productsContainer.css('height', 541);

                    pageDifference = _defaultProdHeight - availableHeight;
                }

                return pageDifference;
            };

            function videoController() {

                var player,
                    timer = false,
                    playerPlaying = false,
                    playerElem = _productsContainer.find('.sou-video');

                // Load Video
                timer = setInterval(function () {
                    if (typeof(YT) === 'object' && YT.hasOwnProperty('Player')) {
                        player = new YT.Player('video-natura-sou', {
                            width: '431',
                            height: '242',
                            videoId: '9R-eREsNmBQ',
                            playerVars: {
                                'enablejsapi': 1,
                                'wmode': 'opaque',
                                'rel': 0,
                                'showinfo': 0
                            },
                            events: {
                                'onReady': videoLoaded,
                                'onStateChange': videoStatusChanged
                            }
                        });
                        clearInterval(timer);
                    }
                }, 500);

                // Activate Video
                playerElem.click(function () {
                    openVideo();
                    playerPlaying = true;
                    if (player) player.playVideo();
                });

                function videoLoaded(event) {
                    if (playerPlaying) {
                        event.target.playVideo();
                    }
                }

                function videoStatusChanged(event) {
                    if (event.data === 0) {
                        closeVideo();
                    }
                }

                function openVideo() {
                    playerElem.find('.sou-cover').fadeOut(500, function () {
                        playerElem.find('.v-container').fadeIn(500);
                    });
                }

                function closeVideo() {
                    playerElem.find('.v-container').fadeOut(500, function () {
                        playerElem.find('.sou-cover').fadeIn(500);
                    });
                }
            };

            /*
             * Animations
             */
            function internalScroll() {
                AppExpSou.scroller(position, position + _scrollRange, function (percent) {
                    //console.warn('=========================')
                    //console.log('position', position)
                    //console.log('position + _scrollRange', position + _scrollRange)
                    //console.log('percent', percent)
                    //console.log('percent', percent)
                    //console.log('top', -Math.round(_pageDifference * (percent / 100)))
                    //console.warn('=========================')


                    _scroll.css('top', -Math.round(_pageDifference * (percent / 100)));
                });
            };

            function animateItens() {

                var list = [
                        AppExpSou.itemScroll({
                            dom: _productsContainer.find('.sou-video'),
                            start: 1000,
                            end: 441,
                            css: 'left',
                            percent: 70
                        }), AppExpSou.itemScroll({
                            dom: _container.find('.condicionador'),
                            start: -170,
                            end: 40,
                            css: 'left',
                            percent: 60
                        }), AppExpSou.itemScroll({
                            dom: _container.find('.videos-list'),
                            start: -100,
                            end: 28,
                            css: 'bottom',
                            percent: 50
                        }), AppExpSou.itemScroll({
                            dom: _container.find('.texto-sem-dilemas'),
                            start: -260,
                            end: 50,
                            css: 'right',
                            percent: 80
                        }), AppExpSou.itemScroll({
                            dom: _container.find('.texto-pra-que'),
                            start: 0,
                            end: 26,
                            css: 'top',
                            percent: 50
                        }), AppExpSou.itemScroll({
                            dom: _container.find('.texto-pra-que'),
                            start: 0,
                            end: 1,
                            css: 'opacity',
                            percent: 50
                        }), AppExpSou.itemScroll({
                            dom: _container.find('.player'),
                            start: 0,
                            end: 1,
                            css: 'opacity',
                            percent: 20
                        }), AppExpSou.itemScroll({
                            dom: _container.find('.player-button'),
                            start: 0,
                            end: 1,
                            css: 'opacity',
                            percent: 20
                        }), AppExpSou.itemScroll({
                            dom: _container.find('.assistindo'),
                            start: 0,
                            end: 1,
                            css: 'opacity',
                            percent: 20
                        }), AppExpSou.itemScroll({
                            dom: _container.find('.hidratante'),
                            start: -175,
                            end: 66,
                            css: 'right',
                            percent: 50
                        }),
                        AppExpSou.itemScroll({
                            dom: _productsContainer.find('.sou-bottle.purple'),
                            start: 0,
                            end: 305,
                            css: 'left',
                            percent: 70
                        }),
                        AppExpSou.itemScroll({
                            dom: _productsContainer.find('.sou-bottle.green-1'),
                            start: 1200,
                            end: 910,
                            css: 'left',
                            percent: 100
                        }),
                        AppExpSou.itemScroll({
                            dom: _productsContainer.find('.sou-bottle.green-2'),
                            start: 1300,
                            end: 877,
                            css: 'left',
                            percent: 85
                        })
                    ],
                    list2 = [
                        AppExpSou.itemScroll({
                            dom: btVerProdutos,
                            start: 275,
                            end: 15,
                            css: 'top',
                            percent: 100
                        }),
                        AppExpSou.itemScroll({
                            dom: btVerProdutos,
                            start: 25,
                            end: 15,
                            css: 'left',
                            percent: 100
                        }),
                        AppExpSou.itemScroll({
                            dom: btVerProdutos,
                            start: 170,
                            end: 94,
                            css: 'width',
                            percent: 100
                        }),
                        AppExpSou.itemScroll({
                            dom: btVerProdutos,
                            start: 170,
                            end: 94,
                            css: 'height',
                            percent: 100
                        })
                    ],
                    btVerProdutosBigTxt = btVerProdutos.find('.big-text'),
                    btVerProdutosSmallTxt = btVerProdutos.find('.small-text');

                AppExpSou.scroller(0, _perfectPosition, function (percent) {
                    $.each(list, function (i, item) {
                        if (percent < item.percent) {

                            if (item.css != 'transform') {
                                item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                            } else {
                                item.dom.css(item.css, 'scale(' + (item.distance * (percent / item.percent)) + item.start + ')');
                            }
                        } else {
                            item.dom.css(item.css, item.end);
                        }
                    });

                    percent >= 100 ? _dropText.fadeIn(300) : _dropText.fadeOut(100);
                    _drop.attr('data-step', Math.floor((40 * percent) / 100));
                });

                AppExpSou.scroller(position, position + _scrollRange + transitionRange - 700, function (percent) {
                    //
                    //$('#expsou-intro').css({
                    //    height: '840'
                    //})
                    $.each(list2, function (i, item) {
                        item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                    });

                    if (percent === 100) {
                        btVerProdutosBigTxt.hide();
                        btVerProdutosSmallTxt.fadeIn(300);
                    } else {
                        btVerProdutosSmallTxt.hide();
                        btVerProdutosBigTxt.fadeIn(300);
                    }

                    btVerProdutos.children('img').hide();
                    btVerProdutos.children('.green-pale').show();
                });
            };

            resize($(window).height());
            AppExpSou.resizeObserver.addObserver(function (wHeight) {
                resize(wHeight);
            });

            videoController();
            internalScroll();
            animateItens();

            return _scrollRange;
        };

        /*
         * Movie "Formula sem excesso"
         *
         */
        AppExpSou.Formula = function (position, windowHeight, transitionRange, btVerProdutos) {

            var _container = $('#expsou-formula'),
                _perfectPosition = position,
                _scrollRange = 1000;

            // Itens
            var list = [{
                dom: _container.find('.sou-cano1'),
                start: -757,
                end: -1435,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-cano2'),
                start: -171,
                end: -720,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-cano3'),
                start: -50,
                end: -100,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-torneira'),
                start: 663,
                end: -200,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-bolha1'),
                start: 300,
                end: -100,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-bolha2'),
                start: 500,
                end: -500,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-bolha3'),
                start: 400,
                end: -200,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-bolha4'),
                start: 500,
                end: 0,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-bolha5'),
                start: 500,
                end: -100,
                css: 'top',
                distance: false,
                percent: 100
            }];

            $.each(list, function (i, item) {
                item.distance = item.end - item.start;
            });

            AppExpSou.scroller(_perfectPosition - transitionRange, _perfectPosition + transitionRange + _scrollRange, function (percent) {
                $.each(list, function (i, item) {
                    if (percent < item.percent) {
                        item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                    } else {
                        item.dom.css(item.css, item.end);
                    }
                });
            });

            var list2 = [{
                dom: _container.find('.sou-liquido'),
                start: 0,
                end: windowHeight + 200,
                css: 'height',
                distance: false,
                percent: 100
            }];

            $.each(list2, function (i, item) {
                item.distance = item.end - item.start;
            });

            AppExpSou.resizeObserver.addObserver(function (value) {
                windowHeight = value;
                list2[0].end = windowHeight + 200;
                list2[0].distance = list2[0].end - list2[0].start;
            });

            AppExpSou.scroller(_perfectPosition + _scrollRange, _perfectPosition + transitionRange + _scrollRange, function (percent) {
                $.each(list2, function (i, item) {
                    item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                });
            });

            AppExpSou.scroller(_perfectPosition, _perfectPosition + transitionRange + _scrollRange, function (percent) {
                if (percent === 0) {
                    btVerProdutos.children('img').hide();
                    btVerProdutos.children('.green-pale').show();
                } else if (percent > 0) {
                    btVerProdutos.children('img').hide();
                    btVerProdutos.children('.pink').show();
                }
            });

            return _scrollRange;
        };


        /****
         *
         *
         *
         *
         */

        /*
         * Movie "Formula sem excesso"
         *
         */

        AppExpSou.Roxo = function (position, windowHeight, transitionRange, btVerProdutos) {
            // 13741, false, 1500, BT

            var _container = $('#chuveirinho'),
                _perfectPosition = position,
                _scrollRange = 1500;

            // Itens
            var list = [{
                dom: _container.find('.hidratante-mao'),
                start: -93,
                end: 24,
                css: 'right',
                distance: false,
                percent: 50
            }, {
                dom: _container.find('.cabelo'),
                start: -100,
                end: 17,
                css: 'left',
                distance: false,
                percent: 50
            }, {
                dom: _container.find('.gambiarra'),
                start: -997,
                end: -787,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.chuva'),
                start: 0,
                end: 3000,
                distance: false,
                percent: 100,
                type: 'chuvaCaindo',
                css: 'background-position-y'
            }, {
                dom: _container.find('.bolinhas'),
                start: 0,
                end: 720,
                distance: false,
                percent: 100,
                type: 'bolinhas',
                css: 'background-position-y'
            }];

            $.each(list, function (i, item) {
                item.distance = item.end - item.start;
            });

            AppExpSou.scroller(_perfectPosition - transitionRange, _perfectPosition + transitionRange + _scrollRange, function (percent) {
                $.each(list, function (i, item) {
                    if (item.type == "chuvaCaindo") {
                        item.dom.css('background-position', '-20px ' + (item.distance * (percent / item.percent)) + item.start + 'px');
                    }

                    if (item.type == "bolinhas") {
                        var value = Math.ceil((item.distance * (percent / item.percent)) + item.start);

                        var percentSum = percent / 100 * 1.5;
                        var newPercent = (percentSum < 100) ? percentSum : percent;
                        var scaleEffect = 'scale(' + newPercent + ', ' + newPercent + ')';

                        item.dom.css(AppExpSou.css3Prefix + 'transform', 'rotate(' + value + 'deg) ');
                        //item.dom.css('opacity', newPercent / 1);
                    }

                    if (percent < item.percent) {
                        item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                    } else {
                        item.dom.css(item.css, item.end);
                    }
                });
            });
            //
            //var list2 = [{
            //    dom: _container.find('.bolinha-1'),
            //    start: 0,
            //    end: 720,
            //    distance: false,
            //    percent: 100,
            //    type: 'bolinha'
            //}, {
            //    dom: _container.find('.bolinha-2'),
            //    start: 0,
            //    end: 720,
            //    distance: false,
            //    percent: 100,
            //    type: 'bolinha'
            //}, {
            //    dom: _container.find('.bolinha-3'),
            //    start: 0,
            //    end: 720,
            //    distance: false,
            //    percent: 100,
            //    type: 'bolinha'
            //}, {
            //    dom: _container.find('.bolinha-4'),
            //    start: 0,
            //    end: 720,
            //    distance: false,
            //    percent: 100,
            //    type: 'bolinha'
            //}, {
            //    dom: _container.find('.bolinha-5'),
            //    start: 0,
            //    end: 720,
            //    distance: false,
            //    percent: 100,
            //    type: 'bolinha'
            //}];
            //
            //
            //$.each(list2, function (i, item) {
            //    item.distance = item.end - item.start;
            //});
            //
            //AppExpSou.resizeObserver.addObserver(function (value) {
            //    windowHeight = value;
            //    list2[0].end = windowHeight + 200;
            //    list2[0].distance = list2[0].end - list2[0].start;
            //});
            //
            //
            //AppExpSou.scroller(_perfectPosition + _scrollRange, _perfectPosition + transitionRange + _scrollRange, function (percent) {
            //    $.each(list2, function (i, item) {
            //
            //
            //        if (item.type == "bolinha") {
            //            var value = Math.ceil((item.distance * (percent / item.percent)) + item.start);
            //            item.dom.css(AppExpSou.css3Prefix + 'transform', 'rotate(' + value + 'deg) ');
            //        } else if (item.type == "chuva") {
            //            var value = Math.ceil((item.distance * (percent / item.percent)) + item.start);
            //            item.dom.css('background-position', '0px ' + value);
            //        } else {
            //            console.error(item.css, (item.distance * (percent / item.percent)) + item.start);
            //            item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
            //        }
            //    });
            //    console.error('CURRENT SCROLL', arguments)
            //
            //if (percent > 0 && percent < 70 && !Rain.started) {
            //    //console.warn('SCROLLER > 90', arguments)
            //
            //    Rain.start();
            //} else if (percent > 70 ) {
            //    //console.warn('SCROLLER MENOR QUE 89', arguments)
            //
            //    Rain.stop()
            //}
//            });

            AppExpSou.scroller(_perfectPosition, _perfectPosition + transitionRange + _scrollRange, function (percent) {
                if (percent === 0) {
                    btVerProdutos.children('img').hide();
                    btVerProdutos.children('.green-pale').show();
                } else if (percent > 0) {
                    btVerProdutos.children('img').hide();
                    btVerProdutos.children('.pink').show();
                }
            });

            return _scrollRange;
        };


        /**
         *
         *
         *
         *
         *
         */
        /*
         * Movie "Processo eficiente"
         *
         */
        AppExpSou.Processo = function (position, windowHeight, transitionRange, btVerProdutos) {

            var _container = $('#expsou-processo');
            _perfectPosition = position,
                _scrollRange = 2000;

            // Itens
            var list = [{
                dom: _container.find('.sou-esteira .sou-inner'),
                start: 0,
                end: -1872,
                css: 'right',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-pointer'),
                start: 0,
                end: 360,
                type: 'pointer',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-engrenagem-1'),
                start: 0,
                end: 720,
                type: 'engrenagem',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-engrenagem-2'),
                start: 0,
                end: -720,
                type: 'engrenagem',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-engrenagem-3'),
                start: 0,
                end: -720,
                type: 'engrenagem',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-stopwatch'),
                start: -100,
                end: 161,
                css: 'right',
                distance: false,
                percent: 20
            }];

            $.each(list, function (i, item) {
                item.distance = item.end - item.start;
            });

            AppExpSou.scroller(_perfectPosition, _perfectPosition + _scrollRange, function (percent) {
                $.each(list, function (i, item) {

                    var value = Math.ceil((item.distance * (percent / item.percent)) + item.start);

                    switch (item.type) {
                        case 'pointer':
                            item.dom.css(AppExpSou.css3Prefix + 'transform', 'rotate(' + value + 'deg) translateY(-9px)');
                            break;
                        case 'engrenagem':
                            item.dom.css(AppExpSou.css3Prefix + 'transform', 'rotate(' + value + 'deg)');
                            break;
                        default:
                            if (percent < item.percent) {
                                item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                            } else {
                                item.dom.css(item.css, item.end);
                            }
                            break;
                    }

                });
            });

            AppExpSou.scroller(_perfectPosition, _perfectPosition + _scrollRange + transitionRange, function (percent) {
                if (percent > 0) {
                    btVerProdutos.children('img').hide();
                    btVerProdutos.children('.blue-pale').show();
                }
            });

            return _scrollRange;
        };

        /*
         * Movie "Uma nova embalagem"
         *
         */
        AppExpSou.Embalagem = function (position, windowHeight, transitionRange, btVerProdutos) {

            var _container = $('#expsou-embalagem');
            _perfectPosition = position,
                _scrollRange = 1500;

            var list = [{
                dom: _container.find('.sou-caminhao'),
                start: 1000,
                end: 507,
                css: 'left',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-camada-1'),
                start: 0,
                end: -1936,
                css: 'right',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-camada-2'),
                start: 0,
                end: -1700,
                css: 'right',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-camada-3'),
                start: 0,
                end: -1390,
                css: 'right',
                distance: false,
                percent: 100
            }];

            $.each(list, function (i, item) {
                item.distance = item.end - item.start;
            });

            AppExpSou.scroller(_perfectPosition - transitionRange, _perfectPosition + _scrollRange, function (percent) {
                $.each(list, function (i, item) {
                    if (percent < item.percent) {
                        item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                    } else {
                        item.dom.css(item.css, item.end);
                    }
                });
            });

            AppExpSou.scroller(_perfectPosition, _perfectPosition + _scrollRange + transitionRange, function (percent) {
                if (percent > 0) {
                    btVerProdutos.children('img').hide();
                    btVerProdutos.children('.orange').show();
                }
            });

            return _scrollRange;
        }

        /*
         * Movie "Essencial"
         *
         */
        AppExpSou.Essencial = function (position, windowHeight, transitionRange, btVerProdutos) {

            var _container = $('#expsou-essencial');
            _perfectPosition = position,
                _scrollRange = 1000;

            var list = [{
                dom: _container.find('.sou-ondas-1'),
                start: 0,
                end: -1243 + windowHeight,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-ondas-2'),
                start: 0,
                end: -900 + windowHeight,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-ondas-3'),
                start: 0,
                end: -900 + windowHeight,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-layer-1'),
                start: -1000,
                end: 1000,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-layer-2'),
                start: -700,
                end: 700,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-layer-3'),
                start: -400,
                end: 400,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-layer-4'),
                start: -250,
                end: 250,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-layer-5'),
                start: -100,
                end: 100,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-gota-escorrendo-1'),
                start: 200,
                end: 300,
                css: 'height',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-gota-escorrendo-2'),
                start: windowHeight - 300,
                end: windowHeight + 100,
                css: 'height',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-gota-escorrendo-3'),
                start: 0,
                end: windowHeight + 500,
                css: 'height',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-gota-escorrendo-4'),
                start: 0,
                end: windowHeight - 200,
                css: 'height',
                distance: false,
                percent: 100
            }];

            $.each(list, function (i, item) {
                item.distance = item.end - item.start;
            });

            AppExpSou.scroller(_perfectPosition - transitionRange, _perfectPosition + transitionRange + _scrollRange, function (percent) {
                $.each(list, function (i, item) {
                    if (percent < item.percent) {
                        item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                    } else {
                        item.dom.css(item.css, item.end);
                    }
                });
            });

            AppExpSou.scroller(_perfectPosition, _perfectPosition + _scrollRange + transitionRange, function (percent) {
                if (percent > 0) {
                    btVerProdutos.children('img').hide();
                    btVerProdutos.children('.green').show();
                }
            });

            return _scrollRange;
        }

        /*
         * Movie "Ãšltima Gota"
         *
         */
        AppExpSou.UltimaGota = function (position, windowHeight, transitionRange, btVerProdutos) {

            var _container = $('#expsou-ultima-gota'),
                _stage = _container.children('.sou-stage'),
                _embalagem = _container.find('.sou-embalagem-principal'),
                _perfectPosition = position,
                _scrollRange = 2000,
                _flipSteps = 5,
                _flipFactor = 100 / _flipSteps,
                _dumpFactor = 100 / 3
            _topCenter = 0,
                _treeTop = 0,
                _dropHeight = 0;

            function resize(wHeight) {

                _topCenter = (wHeight - _stage.height()) / 2;


                //console.log('_topCenter', _topCenter)
                var per = 200 / 1500;
                var dif = -550 - _topCenter;

                _dropHeight = Math.abs(dif - (dif * per));
                _treeTop = _topCenter + 337 - (Math.abs(dif) - _dropHeight);
                _stage.css({marginTop: 0, top: _topCenter});

                $('#expsou-menos-residuo').css('top', _treeTop);

                if (typeof(list2) === 'object') {
                    list2[0].resize({start: _topCenter});
                    list3[0].resize({end: _dropHeight});
                    list4[0].resize({end: wHeight});
                    list4[1].resize({start: _treeTop});
                }
            }

            resize($(window).height());
            AppExpSou.resizeObserver.addObserver(function (wHeight) {
                resize(wHeight);
            });

            /* Embalagens laterais */
            var list1 = [
                AppExpSou.itemScroll({
                    dom: _container.find('.sou-embalagens.left'),
                    start: -390,
                    end: -11,
                    css: 'left',
                    percent: 100
                }),
                AppExpSou.itemScroll({
                    dom: _container.find('.sou-embalagens.right'),
                    start: -400,
                    end: -35,
                    css: 'right',
                    percent: 100
                })
            ];

            AppExpSou.scroller(_perfectPosition - transitionRange, _perfectPosition + 300, function (percent) {
                $.each(list1, function (i, item) {
                    if (percent < item.percent) {
                        item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                    } else {
                        item.dom.css(item.css, item.end);
                    }
                });
            });

            /* Embalagem Flip */
            AppExpSou.scroller(_perfectPosition, _perfectPosition + 300, function (percent, isResize) {
                if (isResize) return;
                _embalagem.attr('data-step', Math.floor(percent / _flipFactor));
            });

            /* PosiÃ§Ã£o do container */
            var list2 = [
                AppExpSou.itemScroll({
                    dom: _stage,
                    start: _topCenter,
                    end: -550,
                    css: 'top',
                    percent: 100
                })
            ];

            AppExpSou.scroller(_perfectPosition + 300, _perfectPosition + 300 + 1500, function (percent) {
                $.each(list2, function (i, item) {
                    if (percent < item.percent) {
                        item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                    } else {
                        item.dom.css(item.css, item.end);
                    }
                });
            });

            /* Gota e ContinuaÃ§Ã£o da embalagem maior*/
            var list3 = [
                AppExpSou.itemScroll({
                    dom: _container.find('.sou-gota'),
                    start: 0,
                    end: _dropHeight,
                    css: 'height',
                    percent: 100
                })
            ];

            AppExpSou.scroller(_perfectPosition + 300 + 200, _perfectPosition + 300 + 900, function (percent, isResize) {
                if (isResize) return;
                _embalagem.attr('data-step', _flipSteps + 1 + Math.floor(percent / _dumpFactor));
            });

            AppExpSou.scroller(_perfectPosition + 300 + 200, _perfectPosition + 300 + 1500, function (percent) {
                $.each(list3, function (i, item) {
                    if (percent < item.percent) {
                        item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                    } else {
                        item.dom.css(item.css, item.end);
                    }
                });
            });

            /* Fim */
            var list4 = [
                AppExpSou.itemScroll({
                    dom: _container.find('#expsou-menos-residuo'),
                    start: 0,
                    end: windowHeight,
                    css: 'height',
                    percent: 100
                }),
                AppExpSou.itemScroll({
                    dom: _container.find('#expsou-menos-residuo'),
                    start: _treeTop,
                    end: 0,
                    css: 'top',
                    percent: 100
                })
            ];

            AppExpSou.scroller(_perfectPosition + 300 + 500, _perfectPosition + 300 + 700 + 1000, function (percent) {
                $.each(list4, function (i, item) {
                    if (percent < item.percent) {
                        item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                    } else {
                        item.dom.css(item.css, item.end);
                    }
                });
            });

            AppExpSou.scroller(_perfectPosition, _perfectPosition + _scrollRange + transitionRange, function (percent) {
                if (percent > 0) {
                    btVerProdutos.children('img').hide();
                    btVerProdutos.children('.blue').show();
                }
            });

            return _scrollRange;
        }

        /*
         * Movie "Menos resÃ­duos"
         *
         */
        AppExpSou.MenosResiduos = function (position, windowHeight, transitionRange, btVerProdutos) {

            var _container = $('#expsou-menos-residuo'),
                _arvore = _container.find('.sou-tree'),
                _perfectPosition = position,
                _scrollRange = 1000;

            resize($(window).height());
            AppExpSou.resizeObserver.addObserver(function (wHeight) {
                resize(wHeight);
            });

            _arvore.hide();

            function resize(wHeight) {

                _arvore.css({
                    top: (wHeight * 0.2),
                    height: wHeight * 0.8
                });

                _container.find('.sou-text').css({top: wHeight - 27});

                $('<img />').load(function () {
                    _arvore.fadeIn(300);
                    _arvore.css('margin-left', -(_arvore.width() / 2) - 38);
                }).attr('src', _arvore.attr('src'));
            }

            var list = [{
                dom: _container.find('.sou-folha-1'),
                start: -343,
                end: 857,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-folha-2'),
                start: -19,
                end: 581,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-folha-3'),
                start: -100,
                end: 800,
                css: 'top',
                distance: false,
                percent: 100
            }, {
                dom: _container.find('.sou-folha-4'),
                start: -693,
                end: 1207,
                css: 'top',
                distance: false,
                percent: 100
            }];

            $.each(list, function (i, item) {
                item.distance = item.end - item.start;
            });

            AppExpSou.scroller(_perfectPosition - transitionRange, _perfectPosition + transitionRange + _scrollRange, function (percent) {
                $.each(list, function (i, item) {
                    item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                });
            });

            AppExpSou.scroller(_perfectPosition, _perfectPosition + _scrollRange + transitionRange, function (percent) {
                if (percent > 0) {
                    btVerProdutos.children('img').hide();
                    btVerProdutos.children('.pink').show();
                }
            });

            return _scrollRange;
        }

        /*
         * Movie "Final Produtos"
         *
         */
        AppExpSou.FinalProdutos = function (position, windowHeight, transitionRange, btVerProdutos) {

            var _container = $('#expsou-menos-final'),
                _perfectPosition = position;


            var list = [{
                dom: _container.find('.sou-mulher'),
                start: -732,
                end: -20,
                css: 'left',
                distance: false,
                percent: 70
            }, {
                dom: _container.find('.sou-mulher'),
                start: 0,
                end: 1,
                css: 'opacity',
                distance: false,
                percent: 50
            },
                {
                    dom: _container.find('.sou-produto-1'),
                    start: -732,
                    end: 43,
                    css: 'right',
                    distance: false,
                    percent: 65
                },
                {
                    dom: _container.find('.sou-produto-2'),
                    start: -782,
                    end: 209,
                    css: 'right',
                    distance: false,
                    percent: 70
                },
                {
                    dom: _container.find('.sou-produto-3'),
                    start: -732,
                    end: 332,
                    css: 'right',
                    distance: false,
                    percent: 75
                }, {
                    dom: _container.find('.sou-produto-4'),
                    start: -732,
                    end: 405,
                    css: 'right',
                    distance: false,
                    percent: 85
                },
                {
                    dom: _container.find('.rosacea'),
                    start: -3,
                    end: 0.50,
                    css: 'opacity',
                    distance: -150,
                    percent: 100
                }, {
                    dom: _container.find('#text-sou-o-que-voce'),
                    start: 0,
                    end: 1,
                    css: 'opacity',
                    distance: false,
                    percent: 100
                },
            ];


            //
            //var list = [{
            //    dom: _container.find('.sou-product-1'),
            //    start: -732,
            //    end: 10,
            //    css: 'left',
            //    distance: false,
            //    percent: 100
            //}, {
            //    dom: _container.find('.sou-product-2'),
            //    start: -543,
            //    end: 167,
            //    css: 'left',
            //    distance: false,
            //    percent: 85
            //}, {
            //    dom: _container.find('.sou-product-3'),
            //    start: -467,
            //    end: 285,
            //    css: 'left',
            //    distance: false,
            //    percent: 70
            //}, {
            //    dom: _container.find('.sou-product-4'),
            //    start: -408,
            //    end: 300,
            //    css: 'right',
            //    distance: false,
            //    percent: 70
            //}, {
            //    dom: _container.find('.sou-product-5'),
            //    start: -574,
            //    end: 163,
            //    css: 'right',
            //    distance: false,
            //    percent: 85
            //}, {
            //    dom: _container.find('.sou-product-6'),
            //    start: -732,
            //    end: 10,
            //    css: 'right',
            //    distance: false,
            //    percent: 100
            //}];

            $.each(list, function (i, item) {
                item.distance = item.end - item.start;
            });

            AppExpSou.scroller(_perfectPosition - transitionRange, _perfectPosition, function (percent) {
                $.each(list, function (i, item) {

                    //if ($(window).height() < 755) {
                    //    _container.css({
                    //        'height': '508px',
                    //        top: '38px'
                    //    });
                    //}

                    if (percent < item.percent) {
                        item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                    } else {
                        item.dom.css(item.css, item.end);
                    }
                });
            });

            AppExpSou.scroller(_perfectPosition, _perfectPosition + _scrollRange + transitionRange, function (percent) {
                if (percent > 0) {
                    btVerProdutos.children('img').hide();
                    btVerProdutos.children('.orange').show();
                }
            });

            return 200;
        }

        /*
         * Movie
         *
         */
        AppExpSou.movie = function (movieDisplay, marginTop, windowHeight, btVerProdutos, watchDisplay) {

            var pageIndex = 0,
                position = marginTop,
                transitionRange = 1000,
                inner = movieDisplay.children('.inner-scroll'),
                transitionController = AppExpSou.pageTransition(inner, windowHeight, transitionRange);

            // Intro
            position += AppExpSou.Intro(position, transitionRange, btVerProdutos);

            // movimentar relogio
            var list = [
                AppExpSou.itemScroll({
                    dom: watchDisplay,
                    start: windowHeight + 10,
                    end: 10,
                    css: 'top',
                    percent: 100
                })
            ];

            watchDisplay.css('top', windowHeight + 10);

            AppExpSou.resizeObserver.addObserver(function (wHeight) {
                list[0].resize({start: wHeight + 10});
            });

            AppExpSou.scroller(position, position + transitionRange, function (percent) {
                $.each(list, function (i, item) {
                    item.dom.css(item.css, (item.distance * (percent / item.percent)) + item.start);
                });
            });

            // Formula
            position += transitionController.add(position);
            position += AppExpSou.Formula(position, windowHeight, transitionRange, btVerProdutos);

            // Processo
            position += transitionController.add(position);
            position += AppExpSou.Processo(position, windowHeight, transitionRange, btVerProdutos);

            // Embalagem
            position += transitionController.add(position);
            position += AppExpSou.Embalagem(position, windowHeight, transitionRange, btVerProdutos);

            // Essencial
            position += transitionController.add(position);
            position += AppExpSou.Essencial(position, windowHeight, transitionRange, btVerProdutos);

            // Ultima Gota
            position += transitionController.add(position);
            position += AppExpSou.UltimaGota(position, windowHeight, transitionRange, btVerProdutos);

            // Menos ResÃ­duos
            position += AppExpSou.MenosResiduos(position, windowHeight, 1500, btVerProdutos);

            position += transitionController.add(position);
            position += AppExpSou.Roxo(position, windowHeight, 800, btVerProdutos);

            // Final Produtos
            position += transitionController.add(position);
            position += AppExpSou.FinalProdutos(position, windowHeight, 500, btVerProdutos);
            //position += transitionController.add(position);

            //console.log('position', position);


            return position;
        };


        AppExpSou.init = function () {
            //
            //if (init) return;
            //init = true;

            var _movieDisplay = $('#experiencia-sou'),
                _movieTop = false,
                _movieRange = false,
                _watch = _movieDisplay.find('.sou-watch'),
                _watchDisplay = _watch.children('.sou-percent'),
                _windowHeight = false,
                _pages = _movieDisplay.find('.page-sou'),
                _btVerProdutos = $('#experiencia-sou .sou-ver-produtos');

            function resize() {
                _windowHeight = $(window).height();
                _movieDisplay.css('height', _windowHeight);
                _pages.css('height', _windowHeight);
                AppExpSou.customScroll.update();
            };

            function percentWatch(top, range) {
                var range = range + 200;
                var timer,
                    count = 0;


                AppExpSou.scroller(top, range, function (percent, isResize) {
                    if (isResize) return;
                    percent === 100 ? playFinishAnim() : stopFinishAnim();
                    _watchDisplay.text(AppExpSou.addLeftZeros(2, Math.floor(percent * 0.6)));
                });

                function playFinishAnim() {

                    _watch.addClass('finished');

                    function light(time) {
                        timer = setTimeout(function () {
                            if (count === 5) {
                                count = 0;
                                light(3000);
                            } else {
                                count++;
                                light(200);
                            }
                            _watch.toggleClass('finished');
                        }, time);
                    };
                    light(200);
                }

                function stopFinishAnim() {
                    count = 0;
                    clearInterval(timer);
                    _watch.removeClass('finished');
                }
            };

            function adjustScreenContainer() {
                $('script', '#header').remove();
                $('#header').wrapAll('<div id="sou-general"></div>');
            }

            return {
                init: function () {

                    // fix content
                    adjustScreenContainer();

                    // show container and calc top
                    _movieTop = 200;
                    _movieDisplay.fadeIn(500);

                    // new movie
                    _movieRange = AppExpSou.movie(_movieDisplay, _movieTop, _windowHeight, _btVerProdutos, _watch);

                    // activate the custom scroll
                    AppExpSou.customScroll.init(_movieTop, _movieRange);

                    // watch "menos carbono"
                    percentWatch(_movieTop, _movieRange);

                    // listener resize
                    $(window).resize(resize);

                    //console.log('Cheguei aqui no resize!');

                    $(window).resize();


                }
            }
        }

    });
}



