'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

gulp.task('sass', function () {
    return gulp.src('./static/sou/assets/css/main.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(concat('main_compiled.css'))
        .pipe(gulp.dest('./static/sou/assets/css/'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./static/sou/assets/css/main.scss', ['sass']);
});
